from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from db.db_config import *
from aiogram.enums import ParseMode
from handlers.localization import Lang
from utils.db_requests import get_all_tickets
from aiogram.exceptions import TelegramBadRequest

async def show_issues_handler(bot, query, state):
    issues = get_all_tickets()

    if issues:
        current_page_data = await state.get_data()
        current_page = current_page_data.get("current_page", 0)
        issues_on_page = issues[current_page*6 : (current_page+1)*6]
        buttons = [[InlineKeyboardButton(text=issue_item.details, callback_data=f"issues_{issue_item.ticket_id}")] for issue_item in issues_on_page]

        if len(issues) > 5:
            buttons.append([
                InlineKeyboardButton(text="Предыдущая", callback_data="prev_ticket_page"),
                InlineKeyboardButton(text="Следующая", callback_data="next_ticket_page")
            ])

        issues_markup = InlineKeyboardMarkup(inline_keyboard=buttons)

        if "last_message_id" in current_page_data:
            # Если есть предыдущее сообщение, редактируем его
            try:
                await bot.edit_message_text(
                    chat_id=query.from_user.id,
                    message_id=current_page_data["last_message_id"],
                    text=Lang.strings["ru"]["tickets_type_success"],
                    reply_markup=issues_markup,
                    parse_mode=ParseMode.MARKDOWN
                )
            except TelegramBadRequest:
                message = await bot.send_message(
                query.from_user.id,
                text=Lang.strings["ru"]["tickets_type_success"],
                reply_markup=issues_markup,
                parse_mode=ParseMode.MARKDOWN
            )
                await state.update_data(last_message_id=message.message_id)
        else:
            # Если предыдущего сообщения нет, отправляем новое сообщение
            message = await bot.send_message(
                query.from_user.id,
                text=Lang.strings["ru"]["tickets_type_success"],
                reply_markup=issues_markup,
                parse_mode=ParseMode.MARKDOWN
            )
            # Сохраняем chat_id и message_id для последующего редактирования
            await state.update_data(last_message_id=message.message_id)

    else:
        await bot.send_message(
            query.from_user.id,
            text=Lang.strings["ru"]["tickets_type_error"],
            parse_mode=ParseMode.MARKDOWN
        )
